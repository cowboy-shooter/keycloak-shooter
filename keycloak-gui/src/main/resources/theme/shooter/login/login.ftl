<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>

<i class="fa fa-sign-in" style="font-size:150px; color: white"></i>
<h1 class="ui header" id="mainHeader">
    <div class="content">
        Login
    </div>
</h1>
<div class="ui inverted segment">
    <form class="ui inverted form" id="form" action="${url.loginAction}" method="post">
            <div class="field">
                <label>Username or E-Mail</label>
                <input placeholder="Username or E-Mail" name="username" type="text">
            </div>
            <div class="field">
                <label>Password</label>
                <input placeholder="Password" name="password" type="password" autocomplete="off">
            </div>
        <#if login.rememberMe??>
            <div class="ui checked checkbox">
                <input type="checkbox" checked="" id="rememberMe" name="rememberMe" checked>
                <label>${msg("rememberMe")}</label>
            </div>
        <#else>
            <div class="ui checkbox">
                <input type="checkbox" checked="" id="rememberMe" name="rememberMe">
                <label>${msg("rememberMe")}</label>
            </div>
        </#if>
        <br/>
        <br/>

        <button class="ui inverted green basic fluid massive button" id="submit">
            Submit
        </button>
    </form>

    <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
        <div id="kc-registration">
            <span>${msg("noAccount")} <a tabindex="6" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
        </div>
    </#if>
</div>

</@layout.registrationLayout>
