<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <i class="fa fa-user-plus" style="font-size:150px; color: white"></i>
    <h1 class="ui header" id="mainHeader">
        <div class="content">
            Register
        </div>
    </h1>
    <div class="ui inverted segment">
        <form class="ui inverted form" id="form" action="${url.loginAction}" method="post">
            <div class="required field">
                <label>First Name</label>
                <input placeholder="First Name" name="firstName" type="text" value="${(register.formData.firstName!'')}" required>
            </div>
            <div class="required field">
                <label>Last Name</label>
                <input placeholder="Last Name" name="lastName" type="text" value="${(register.formData.lastName!'')}" required>
            </div>
            <div class="required field">
                <label>E-Mail</label>
                <input placeholder="E-Mail" name="email" type="text" value="${(register.formData.email!'')}" autocomplete="email" required>
            </div>
            <div class="required field">
                <label>Username</label>
                <input placeholder="Username" name="username" type="text" value="${(register.formData.username!'')}" autocomplete="username" required>
            </div>
            <div class="required field">
                <label>Password</label>
                <input placeholder="Password" name="password" type="password" autocomplete="off" required>
            </div>
            <div class="required field">
                <label>Password Confirmation</label>
                <input placeholder="Password Confirmation" name="password-confirm" type="password" autocomplete="off" required>
            </div>
            <#if login.rememberMe??>
                <div class="ui checked checkbox">
                    <input type="checkbox" checked="" id="rememberMe" name="rememberMe" checked>
                    <label>${msg("rememberMe")}</label>
                </div>
            <#else>
                <div class="ui checkbox">
                    <input type="checkbox" checked="" id="rememberMe" name="rememberMe">
                    <label>${msg("rememberMe")}</label>
                </div>
            </#if>
            <br/>
            <br/>

            <button class="ui inverted green basic fluid massive button" id="submit">
                Submit
            </button>
        </form>

        <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
            <div class="${properties.kcFormOptionsWrapperClass!}">
                <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
            </div>
        </div>
    </div>
</@layout.registrationLayout>
