<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayWide=false>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title>${msg("loginTitle",(realm.displayName!''))}</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
     <#if properties.scripts?has_content>
         <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
         </#list>
     </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
</head>

<body class="${properties.kcBodyClass!}">
<div class="ui two column grid" id="mainGrid">
    <div class="row">
        <div class="column">
            <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                <div class="alert alert-${message.type}">
                    <#if message.type = 'success'>
                        <div class="ui success message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                    <#if message.type = 'warning'>
                        <div class="ui warning message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                    <#if message.type = 'error'>
                        <div class="ui error message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                    <#if message.type = 'info'>
                        <div class="ui info message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                </div>
            </#if>
        </div>
    </div>
    <div class="row">
        <div class="column"><#nested "form"></div>
    </div>
</div>

</body>
</html>
</#macro>
