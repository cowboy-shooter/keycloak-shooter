<#macro mainLayout active bodyClass>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <title>${msg("accountManagementTitle")}</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico">
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script type="text/javascript" src="${url.resourcesPath}/${script}"></script>
        </#list>
    </#if>
</head>
<body class="admin-console user ${bodyClass}">

<div class="ui two column grid" id="mainGrid">
    <div class="row">
        <div class="column">
            <#if message?has_content>
                <div class="alert alert-${message.type}">
                    <#if message.type = 'success'>
                        <div class="ui success message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                    <#if message.type = 'warning'>
                        <div class="ui warning message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                    <#if message.type = 'error'>
                        <div class="ui error message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                    <#if message.type = 'info'>
                        <div class="ui info message">
                            <div class="header">
                                ${kcSanitize(message.summary)?no_esc}
                            </div>
                        </div>
                    </#if>
                </div>
            </#if>
        </div>
    </div>
    <div class="row">
        <div class="column"><#nested "content"></div>
    </div>
</div>

</body>
</html>
</#macro>