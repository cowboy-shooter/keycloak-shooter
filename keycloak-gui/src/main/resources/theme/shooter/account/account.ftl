<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <i class="fa fa-user" style="font-size:150px; color: white"></i>
    <h1 class="ui header" id="mainHeader">
        <div class="content">
            Account Management
        </div>
    </h1>
    <div class="ui inverted segment">
        <form class="ui inverted form" id="form" action="${url.accountUrl}" method="post">
            <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">
            <div class="required field">
                <label>Username</label>
                <input placeholder="Username" name="username" type="text" disabled="disabled" value="${(account.username!'')}" required>
            </div>
            <div class="required field">
                <label>E-Mail</label>
                <input placeholder="E-Mail" name="email" type="text" value="${(account.email!'')}" required>
            </div>
            <div class="required field">
                <label>First Name</label>
                <input placeholder="First Name" name="firstName" type="text" value="${(account.firstName!'')}" required>
            </div>
            <div class="required field">
                <label>Last Name</label>
                <input placeholder="Last Name" name="lastName" type="text" value="${(account.lastName!'')}" required>
            </div>
            <br/>
            <br/>

            <button class="ui inverted green basic fluid massive button" id="submit" name="submitAction" value="Save">
                Save Changes
            </button>
            <#if referrer.url??>
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                        <span><a href="${referrer.url}">${kcSanitize(msg("backToApplication"))?no_esc}</a></span>
                    </div>
                </div>
            </#if>
        </form>
    </div>

    <#--<div class="row">-->
        <#--<div class="col-md-10">-->
            <#--<h2>${msg("editAccountHtmlTitle")}</h2>-->
        <#--</div>-->
        <#--<div class="col-md-2 subtitle">-->
            <#--<span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>-->
        <#--</div>-->
    <#--</div>-->

    <#--<form action="${url.accountUrl}" class="form-horizontal" method="post">-->

        <#--<input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">-->

        <#--<#if !realm.registrationEmailAsUsername>-->
            <#--<div class="form-group ${messagesPerField.printIfExists('username','has-error')}">-->
                <#--<div class="col-sm-2 col-md-2">-->
                    <#--<label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>-->
                <#--</div>-->

                <#--<div class="col-sm-10 col-md-10">-->
                    <#--<input type="text" class="form-control" id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>-->
                <#--</div>-->
            <#--</div>-->
        <#--</#if>-->

        <#--<div class="form-group ${messagesPerField.printIfExists('email','has-error')}">-->
            <#--<div class="col-sm-2 col-md-2">-->
            <#--<label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>-->
            <#--</div>-->

            <#--<div class="col-sm-10 col-md-10">-->
                <#--<input type="text" class="form-control" id="email" name="email" autofocus value="${(account.email!'')}"/>-->
            <#--</div>-->
        <#--</div>-->

        <#--<div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">-->
            <#--<div class="col-sm-2 col-md-2">-->
                <#--<label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>-->
            <#--</div>-->

            <#--<div class="col-sm-10 col-md-10">-->
                <#--<input type="text" class="form-control" id="firstName" name="firstName" value="${(account.firstName!'')}"/>-->
            <#--</div>-->
        <#--</div>-->

        <#--<div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">-->
            <#--<div class="col-sm-2 col-md-2">-->
                <#--<label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>-->
            <#--</div>-->

            <#--<div class="col-sm-10 col-md-10">-->
                <#--<input type="text" class="form-control" id="lastName" name="lastName" value="${(account.lastName!'')}"/>-->
            <#--</div>-->
        <#--</div>-->

        <#--<div class="form-group">-->
            <#--<div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">-->
                <#--<div class="">-->
                    <#--<#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>-->
                    <#--<button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>-->
                    <#--<button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>-->
                <#--</div>-->
            <#--</div>-->
        <#--</div>-->
    <#--</form>-->

</@layout.mainLayout>