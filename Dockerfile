FROM maven:latest as build

LABEL maintainer="Marcel Ouška <marcel.ouska@gmail.com>"

LABEL io.k8s.description="Keycloak image for Cowboy Shooter" \
      io.k8s.display-name="Keycloak Shooter" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="keycloak"

WORKDIR /opt/adapter/keycloak-shooter
COPY . .
RUN mvn clean install

FROM jboss/keycloak:latest

COPY --from=build /opt/adapter/keycloak-shooter/keycloak-builder/target/*.tar /opt/adapter/work/
USER root

RUN chmod -R 777 /opt/adapter/work/
RUN chown -R 1000:1000 /opt/adapter/work/

RUN chmod -R 777 ${JBOSS_HOME}
RUN chown -R 1000:1000 ${JBOSS_HOME}

USER jboss

RUN tar -C /opt/adapter/work/ -xvf /opt/adapter/work/*.tar
RUN cp -r /opt/adapter/work/jboss/* ${JBOSS_HOME}/

RUN ${JBOSS_HOME}/bin/add-user-keycloak.sh -u admin -p admin
RUN ${JBOSS_HOME}/bin/jboss-cli.sh --file=/opt/adapter/work/cli/jboss-standalone-xml-config.cli
RUN ${JBOSS_HOME}/bin/jboss-cli.sh --file=/opt/adapter/work/cli/jboss-standalone-xml-config-ha.cli
RUN ${JBOSS_HOME}/bin/jboss-cli.sh --file=/opt/adapter/work/cli/jboss-standalone-xml-config-other.cli
RUN ${JBOSS_HOME}/bin/jboss-cli.sh --file=/opt/adapter/work/cli/jboss-standalone-xml-config-other-ha.cli

RUN rm -rf ${JBOSS_HOME}/standalone/log/*
RUN rm -rf ${JBOSS_HOME}/standalone/configuration/standalone_xml_history


ENTRYPOINT [ "/opt/jboss/tools/docker-entrypoint.sh" ]

CMD ["-b", "0.0.0.0"]
#sudo docker image build -t keycloak-shooter .
#sudo docker run -p 127.0.0.1:8080:8080 keycloak-shooter